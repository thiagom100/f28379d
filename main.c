//#############################################################################
// Trabalho III EEE929
// Autores: Renata Cristina da Silva e Thiago Martins Firmo
//#############################################################################

#include "Perifericos.h"
#include "driverlib.h"
#include "device.h"
#include "F2837xD_device.h"


#define CMPA_max  2250U //10% duty
#define CMPA_min  250U  //90% duty
#define TBPRD     2500U

float VAR_Renata1,VAR_Renata2,VAR_Renata3;
Uint16 CONT_epwm4,CONT_epwm5,CONT_epwm6;

__interrupt void epwm4ISR(void);
__interrupt void epwm5ISR(void);
__interrupt void epwm6ISR(void);

void main(void)
{

    Device_init();                              // Inicialização do sistema

    Interrupt_initModule();                     // Inicia PIE e limpa os registradores PIE. Desabilita iterrupcoes da CPU
                                                // e limpa todas as flags da interrupção da CPU.
    Interrupt_initVectorTable();                // Inicializa PIE vector table

    configEPWMGPIO();                           // Configuração dos pinos de PWM

    configEPWM_param();                         // Configuração dos parâmetros do módulo ePWM

    configEPWM_db();                            // Configuração do deadband

    configEPWM_int();                           // Configuração do tipo de interrupção do módulo ePWM

    Interrupt_register(INT_EPWM4, &epwm4ISR);
    Interrupt_register(INT_EPWM5, &epwm5ISR);
    Interrupt_register(INT_EPWM6, &epwm6ISR);

    while(1){

    }
}

__interrupt void epwm4ISR(void)
{
    CONT_epwm4=(Uint16)(1-VAR_Renata1)*TBPRD;
    EPWM_setCounterCompareValue(EPWM4_BASE,EPWM_COUNTER_COMPARE_A,CONT_epwm4);      //EPwm4Regs.CMPA.bit.CMPA = valor;
    EPWM_clearEventTriggerInterruptFlag(EPWM4_BASE);
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP3);
}

__interrupt void epwm5ISR(void)
{
    CONT_epwm5=(Uint16)(1-VAR_Renata2)*TBPRD;
    EPWM_setCounterCompareValue(EPWM5_BASE,EPWM_COUNTER_COMPARE_A,CONT_epwm5);      //EPwm5Regs.CMPA.bit.CMPA = valor;
    EPWM_clearEventTriggerInterruptFlag(EPWM5_BASE);
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP3);
}

__interrupt void epwm6ISR(void)
{
    CONT_epwm6=(Uint16)(1-VAR_Renata3)*TBPRD;
    EPWM_setCounterCompareValue(EPWM6_BASE,EPWM_COUNTER_COMPARE_A,CONT_epwm6);      //EPwm6Regs.CMPA.bit.CMPA = valor;
    EPWM_clearEventTriggerInterruptFlag(EPWM6_BASE);
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP3);
}
