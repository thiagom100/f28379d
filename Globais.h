/*
 * Globais.h
 *
 *  Created on: 15 de mar de 2021
 *      Author: thiago
 */

#ifndef GLOBAIS_H_
#define GLOBAIS_H_

EPWM_SignalParams pwmSignal =
            {20000, 0.5f, 0.5f, true, DEVICE_SYSCLK_FREQ, SYSCTL_EPWMCLK_DIV_2,
            EPWM_COUNTER_MODE_UP_DOWN, EPWM_CLOCK_DIVIDER_1,
            EPWM_HSCLOCK_DIVIDER_1};            // Configuração dos parâmetros do PWM (freq, dutyA, dutyB, A inv B, ...)





#endif /* GLOBAIS_H_ */
