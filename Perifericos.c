//#############################################################################
// Trabalho III EEE929
// Autores: Renata Cristina da Silva e Thiago Martins Firmo
//#############################################################################

#include "Perifericos.h"

EPWM_SignalParams pwmSignal =
            {20000, 0.5f, 0.5f, true, DEVICE_SYSCLK_FREQ, SYSCTL_EPWMCLK_DIV_2,
            EPWM_COUNTER_MODE_UP_DOWN, EPWM_CLOCK_DIVIDER_1,
            EPWM_HSCLOCK_DIVIDER_1};            // Configura��o dos par�metros do PWM (freq, dutyA, dutyB, A inv B, ...)



// Fun��o de configura��o dos pinos de PWM - Correspondem aos pinos 5-10 do header J8

void configEPWMGPIO(void)
{

    GPIO_setPadConfig(6, GPIO_PIN_TYPE_STD);
    GPIO_setPinConfig(GPIO_6_EPWM4A);

    GPIO_setPadConfig(7, GPIO_PIN_TYPE_STD);
    GPIO_setPinConfig(GPIO_7_EPWM4B);

    GPIO_setPadConfig(8, GPIO_PIN_TYPE_STD);
    GPIO_setPinConfig(GPIO_8_EPWM5A);

    GPIO_setPadConfig(9, GPIO_PIN_TYPE_STD);
    GPIO_setPinConfig(GPIO_9_EPWM5B);

    GPIO_setPadConfig(10, GPIO_PIN_TYPE_STD);
    GPIO_setPinConfig(GPIO_10_EPWM6A);

    GPIO_setPadConfig(11, GPIO_PIN_TYPE_STD);
    GPIO_setPinConfig(GPIO_11_EPWM6B);
}


void configEPWM_param(void)
{
    // INICIO: TRM - 15.4.4 Phase Locking the Time-Base Clocks of Multiple ePWM Modules

    SysCtl_disablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);      // desabilita o sincronismo de clock dos m�dulos epwm

    EPWM_configureSignal(EPWM4_BASE, &pwmSignal);               // configura o PWM4 conforme pwmSignal
    EPWM_configureSignal(EPWM5_BASE, &pwmSignal);               // configura o PWM5 conforme pwmSignal
    EPWM_configureSignal(EPWM6_BASE, &pwmSignal);               // configura o PWM6 conforme pwmSignal

    EPWM_disablePhaseShiftLoad(EPWM4_BASE);                     // desabilita o deslocamento de fase para o PWM4
    EPWM_setPhaseShift(EPWM4_BASE, 0U);                         // fase de PWM4 = 0, "refer�ncia"

    EPWM_setSyncOutPulseMode(EPWM4_BASE, EPWM_SYNC_OUT_PULSE_ON_COUNTER_ZERO);  // configura sincronismo do clock

    configurePhase(EPWM5_BASE, EPWM4_BASE, 0U);                 // configura a fase do PWM5, baseando-se na refer�ncia PWM4
    configurePhase(EPWM6_BASE, EPWM4_BASE, 0U);                 // configura a fase do PWM6, baseando-se na refer�ncia PWM4

    EPWM_enablePhaseShiftLoad(EPWM5_BASE);                      // habilita sincronismo de fase para o PWM5
    EPWM_enablePhaseShiftLoad(EPWM6_BASE);                      // habilita sincronismo de fase para o PWM6

    SysCtl_enablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);       // habilita o sincronismo de clock dos m�dulos epwm

    // FIM: TRM - 15.4.4 Phase Locking the Time-Base Clocks of Multiple ePWM Modules
}


void configurePhase(uint32_t base, uint32_t masterBase, uint16_t phaseVal)
{
    uint32_t readPrdVal, phaseRegVal;

    readPrdVal = EPWM_getTimeBasePeriod(masterBase);

    if((HWREGH(base + EPWM_O_TBCTL) & 0x3U) == EPWM_COUNTER_MODE_UP_DOWN)
    {
        phaseRegVal = (2U * readPrdVal * phaseVal) / 360U;
    }
    else if((HWREGH(base + EPWM_O_TBCTL) & 0x3U) < EPWM_COUNTER_MODE_UP_DOWN)
    {
        phaseRegVal = (readPrdVal * phaseVal) / 360U;
    }

    EPWM_selectPeriodLoadEvent(base, EPWM_SHADOW_LOAD_MODE_SYNC);
    EPWM_setPhaseShift(base, phaseRegVal);
    EPWM_setTimeBaseCounter(base, phaseRegVal);
}

void configEPWM_db(void) // Active High Complementary (AHC)
{
    EPWM_setRisingEdgeDeadBandDelayInput(EPWM4_BASE, EPWM_DB_INPUT_EPWMA);
    EPWM_setFallingEdgeDeadBandDelayInput(EPWM4_BASE, EPWM_DB_INPUT_EPWMA);
    EPWM_setRisingEdgeDeadBandDelayInput(EPWM5_BASE, EPWM_DB_INPUT_EPWMA);
    EPWM_setFallingEdgeDeadBandDelayInput(EPWM5_BASE, EPWM_DB_INPUT_EPWMA);
    EPWM_setRisingEdgeDeadBandDelayInput(EPWM6_BASE, EPWM_DB_INPUT_EPWMA);
    EPWM_setFallingEdgeDeadBandDelayInput(EPWM6_BASE, EPWM_DB_INPUT_EPWMA);

    EPWM_setFallingEdgeDelayCount(EPWM4_BASE, 100);
    EPWM_setRisingEdgeDelayCount(EPWM4_BASE, 100);
    EPWM_setFallingEdgeDelayCount(EPWM5_BASE, 100);
    EPWM_setRisingEdgeDelayCount(EPWM5_BASE, 100);
    EPWM_setFallingEdgeDelayCount(EPWM6_BASE, 100);
    EPWM_setRisingEdgeDelayCount(EPWM6_BASE, 100);

    EPWM_setDeadBandDelayPolarity(EPWM4_BASE, EPWM_DB_RED, EPWM_DB_POLARITY_ACTIVE_HIGH);
    EPWM_setDeadBandDelayPolarity(EPWM4_BASE, EPWM_DB_FED, EPWM_DB_POLARITY_ACTIVE_LOW);
    EPWM_setDeadBandDelayPolarity(EPWM5_BASE, EPWM_DB_RED, EPWM_DB_POLARITY_ACTIVE_HIGH);
    EPWM_setDeadBandDelayPolarity(EPWM5_BASE, EPWM_DB_FED, EPWM_DB_POLARITY_ACTIVE_LOW);
    EPWM_setDeadBandDelayPolarity(EPWM6_BASE, EPWM_DB_RED, EPWM_DB_POLARITY_ACTIVE_HIGH);
    EPWM_setDeadBandDelayPolarity(EPWM6_BASE, EPWM_DB_FED, EPWM_DB_POLARITY_ACTIVE_LOW);

    EPWM_setDeadBandDelayMode(EPWM4_BASE, EPWM_DB_RED, true);
    EPWM_setDeadBandDelayMode(EPWM4_BASE, EPWM_DB_FED, true);
    EPWM_setDeadBandDelayMode(EPWM5_BASE, EPWM_DB_RED, true);
    EPWM_setDeadBandDelayMode(EPWM5_BASE, EPWM_DB_FED, true);
    EPWM_setDeadBandDelayMode(EPWM6_BASE, EPWM_DB_RED, true);
    EPWM_setDeadBandDelayMode(EPWM6_BASE, EPWM_DB_FED, true);

    EPWM_setDeadBandOutputSwapMode(EPWM4_BASE, EPWM_DB_OUTPUT_A, false);
    EPWM_setDeadBandOutputSwapMode(EPWM4_BASE, EPWM_DB_OUTPUT_B, false);
    EPWM_setDeadBandOutputSwapMode(EPWM5_BASE, EPWM_DB_OUTPUT_A, false);
    EPWM_setDeadBandOutputSwapMode(EPWM5_BASE, EPWM_DB_OUTPUT_B, false);
    EPWM_setDeadBandOutputSwapMode(EPWM6_BASE, EPWM_DB_OUTPUT_A, false);
    EPWM_setDeadBandOutputSwapMode(EPWM6_BASE, EPWM_DB_OUTPUT_B, false);
}

void configEPWM_int(void){
    //
        // Interrupt where we will change the Compare Values
        // Select INT on Time base counter zero event,
        // Enable INT, generate INT on 1rd event
        //
        EPWM_setInterruptSource(EPWM4_BASE, EPWM_INT_TBCTR_ZERO);
        EPWM_enableInterrupt(EPWM4_BASE);
        EPWM_setInterruptEventCount(EPWM4_BASE, 1U);

        EPWM_setInterruptSource(EPWM5_BASE, EPWM_INT_TBCTR_ZERO);
        EPWM_enableInterrupt(EPWM5_BASE);
        EPWM_setInterruptEventCount(EPWM5_BASE, 1U);

        EPWM_setInterruptSource(EPWM6_BASE, EPWM_INT_TBCTR_ZERO);
        EPWM_enableInterrupt(EPWM6_BASE);
        EPWM_setInterruptEventCount(EPWM6_BASE, 1U);
}
