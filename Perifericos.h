//#############################################################################
// Trabalho III EEE929
// Autores: Renata Cristina da Silva e Thiago Martins Firmo
//#############################################################################

#ifndef PERIFERICOS_H_
#define PERIFERICOS_H_
#include "driverlib.h"
#include "device.h"


void configEPWMGPIO(void);
void configurePhase(uint32_t base, uint32_t masterBase, uint16_t phaseVal);
void configEPWM_param(void);
void configEPWM_db(void);
void configEPWM_int(void);

#endif
